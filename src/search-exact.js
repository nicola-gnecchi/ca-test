function searchIndex(array, value, start = 0) {
  if (!array.length) return -1;
  let end = array.length - 1;
  if (value < array[start].x || value > array[end].x) return -1;

  while (start <= end) {
    const middle = Math.floor((start + end) * 0.5);
    const middleValue = array[middle].x;
    if (middleValue === value) return middle;
    if (middleValue > value) end = middle - 1;
    else start = middle + 1;
  }

  return -1;
}

module.exports = searchIndex;
