const Series = require('./series');
const inputData = require('../data.json');

const scoreSeries = Series.getSeriesFromData(
  inputData,
  'aggregation-overall',
  'score'
);

const extraSeries = Series.getSeriesFromData(
  inputData,
  'aggregation-overall',
  'extra'
);

const result1 = Series.getWindowExact(
  scoreSeries.series,
  '2015-08-19T14:00:19.352000Z',
  '2015-10-12T07:27:47.493000Z'
);
const result2 = Series.getWindowBetween(
  scoreSeries.series,
  '2014-08-19T14:00:19.352000Z',
  '2015-10-12T07:27:47.493001Z'
);

console.log('existing', result1);
console.log('between', result2);

const graphData = Series.getScoreSeriesWithExtra(
  inputData,
  'aggregation-overall',
  '2014-08-19T14:00:19.352000Z',
  '2015-10-12T07:27:47.493001Z'
);

const first = graphData.getAt(0);
const last = graphData.getAt(2);

console.log('first', first);
console.log('last', last);

const graphMap = graphData.extractMap();

graphMap.forEach(({ key, tooltip }) =>
  console.log('[ format for', key, ']', tooltip)
);

const gotByKey = graphMap.get('2015-08-19T14:00:19.352000Z');
console.log('get by key', gotByKey);
