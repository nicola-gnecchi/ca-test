const Series = require('./series');
const inputData = require('../data.json');

const at = (time, value) => ({ x: time, y: value });

describe('getSeriesFromData', () => {
  test('it gets series by slug and key', () => {
    const series = Series.getSeriesFromData(
      inputData,
      'aggregation-overall',
      'score'
    );
    expect(series.key).toEqual('score');
  });

  test('it returns null if not found', () => {
    const series = Series.getSeriesFromData(
      inputData,
      'undefined-slug',
      'score'
    );
    expect(series).toBeNull();
  });
});

describe('getWindowExact', () => {
  test('return the series between two dates', () => {
    const series = [at('2'), at('2'), at('4'), at('5'), at('6'), at('7')];
    expect(Series.getWindowExact(series, '4', '6')).toHaveLength(3);
  });
  test('it works with input data', () => {
    const dateFrom = '2015-08-19T14:00:19.352000Z';
    const dateTo = '2015-10-12T07:27:47.493000Z';
    const expected = [
      { y: 282, x: '2015-08-19T14:00:19.352000Z' },
      { y: 227, x: '2015-10-08T14:45:31.991000Z' },
      { y: 185, x: '2015-10-12T07:27:47.493000Z' },
    ];
    const scoreSeries = inputData.data[0].details[0].series;
    expect(Series.getWindowExact(scoreSeries, dateFrom, dateTo)).toEqual(
      expected
    );
  });

  test('it works with other data', () => {
    const dateFrom = '2017-01-17T17:17:06.588498Z';
    const dateTo = '2017-01-17T17:17:06.725851Z';
    const expected = [
      { y: 391, x: '2017-01-17T17:17:06.588498Z' },
      { y: 391, x: '2017-01-17T17:17:06.593704Z' },
      { y: 391, x: '2017-01-17T17:17:06.616142Z' },
      { y: 391, x: '2017-01-17T17:17:06.679473Z' },
      { y: 391, x: '2017-01-17T17:17:06.719200Z' },
      { y: 391, x: '2017-01-17T17:17:06.725851Z' },
    ];
    const scoreSeries = inputData.data[0].details[0].series;
    expect(Series.getWindowExact(scoreSeries, dateFrom, dateTo)).toEqual(
      expected
    );
  });
});

describe('getWindowBetween', () => {
  test('it works with input data', () => {
    const dateFrom = '2014-08-19T14:00:19.352000Z';
    const dateTo = '2015-10-12T07:27:47.493000Z';
    const expected = [
      { y: 282, x: '2015-08-19T14:00:19.352000Z' },
      { y: 227, x: '2015-10-08T14:45:31.991000Z' },
      { y: 185, x: '2015-10-12T07:27:47.493000Z' },
    ];
    const scoreSeries = inputData.data[0].details[0].series;
    expect(Series.getWindowBetween(scoreSeries, dateFrom, dateTo)).toEqual(
      expected
    );
  });

  test('it works with other data', () => {
    const dateFrom = '2017-01-17T17:17:06.588497Z';
    const dateTo = '2017-01-17T17:17:06.725852Z';
    const expected = [
      { y: 391, x: '2017-01-17T17:17:06.588498Z' },
      { y: 391, x: '2017-01-17T17:17:06.593704Z' },
      { y: 391, x: '2017-01-17T17:17:06.616142Z' },
      { y: 391, x: '2017-01-17T17:17:06.679473Z' },
      { y: 391, x: '2017-01-17T17:17:06.719200Z' },
      { y: 391, x: '2017-01-17T17:17:06.725851Z' },
    ];
    const scoreSeries = inputData.data[0].details[0].series;
    expect(Series.getWindowBetween(scoreSeries, dateFrom, dateTo)).toEqual(
      expected
    );
  });

  test('it works between', () => {
    const series = [at(2), at(3), at(4), at(5), at(6), at(7)];
    expect(Series.getWindowBetween(series, 4, 5)).toHaveLength(2);
    expect(Series.getWindowBetween(series, 2.5, 5.5)).toHaveLength(3);
    expect(Series.getWindowBetween(series, 1, 18)).toHaveLength(6);
    expect(Series.getWindowBetween(series, 6.5, 10)).toHaveLength(1);
    expect(Series.getWindowBetween(series, 0, 2.5)).toHaveLength(1);
  });

  test('it deals with empty', () => {
    const series = [at(2), at(3), at(4), at(5), at(6), at(7)];
    expect(Series.getWindowBetween(series, 0, 1)).toHaveLength(0);
    expect(Series.getWindowBetween(series, 7, 7)).toHaveLength(1);
    expect(Series.getWindowBetween(series, 7, 8)).toHaveLength(1);
    expect(Series.getWindowBetween(series, 8, 10)).toHaveLength(0);
  });
});
