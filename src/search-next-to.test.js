const searchNextTo = require('./search-next-to');

const at = (time, value) => ({ x: time, y: value });

test('it finds a slice beetween times', () => {
  const series = [at(1), at(2), at(4), at(5), at(8), at(9)];
  // simple
  expect(searchNextTo(series, 3, 0, true)).toEqual(2);
  expect(searchNextTo(series, 3, 0, false)).toEqual(1);
  expect(searchNextTo(series, 7, 0, true)).toEqual(4);
  expect(searchNextTo(series, 7, 0, false)).toEqual(3);
  expect(searchNextTo(series, 1.5, 0, true)).toEqual(1);
  expect(searchNextTo(series, 8.5, 0, true)).toEqual(5);
  expect(searchNextTo(series, 9.5, 0, false)).toEqual(5);

  // sides
  expect(searchNextTo(series, 0, 0, true)).toEqual(0);
  expect(searchNextTo(series, 12, 0, true)).toEqual(-1);
  expect(searchNextTo(series, 12, 0, false)).toEqual(5);
  expect(searchNextTo(series, -1, 0, false)).toEqual(-1);

  // found
  expect(searchNextTo(series, 1, 0, true)).toEqual(0);
  expect(searchNextTo(series, 9, 0, true)).toEqual(5);

  // from
  expect(searchNextTo(series, 1, 2, true)).toEqual(2);
  expect(searchNextTo(series, 5, 2, true)).toEqual(3);
  expect(searchNextTo(series, 1, 2, false)).toEqual(-1);
});
