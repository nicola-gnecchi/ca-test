const data = require('../data.json');
const searchExactIndex = require('./search-exact');
const searchNextTo = require('./search-next-to');

function getWindowExact(series, dateFrom, dateTo) {
  const fromIndex = searchExactIndex(series, dateFrom, 0);
  const toIndex = searchExactIndex(series, dateTo, fromIndex);
  return series.slice(fromIndex, toIndex + 1);
}

function getIndexesBetween(series, dateFrom, dateTo) {
  const fromIndex = searchNextTo(series, dateFrom, 0, true);
  if (fromIndex < 0) return [-1, -1];
  const toIndex = searchNextTo(series, dateTo, fromIndex, false);
  return [fromIndex, toIndex];
}

function getWindowBetween(series, dateFrom, dateTo) {
  const [fromIndex, toIndex] = getIndexesBetween(series, dateFrom, dateTo);
  return getSlice(series, fromIndex, toIndex);
}

function getSlice(series, fromIndex, toIndex) {
  if (fromIndex < 0 || toIndex < 0) return [];
  return series.slice(fromIndex, toIndex + 1);
}

function getSeriesFromData(data, slug, key) {
  const set = data.data.find((item) => item.slug === slug);
  if (!set) return null;
  const series = set.details.find((item) => item.key === key);
  return series;
}

function getScoreSeriesWithExtra(data, slug, dateFrom, dateTo) {
  const set = data.data.find((item) => item.slug === slug);
  if (!set) return null;
  const scoreSeries = set.details.find((item) => item.key === 'score');
  const extraSeries = set.details.find((item) => item.key === 'extra');
  const [fromIndex, toIndex] = getIndexesBetween(
    scoreSeries.series,
    dateFrom,
    dateTo
  );
  return {
    score: getSlice(scoreSeries.series, fromIndex, toIndex),
    extra: getSlice(extraSeries.series, fromIndex, toIndex),
    getAt: function(index) {
      const score = this.score[index];
      const extra = this.extra[index];
      return {
        key: score.x,
        score: score.y,
        extra: extra.y,
        tooltip: `${extra.y.quiz_session_type} with a score of ${score.y} and priority ${extra.y.priority}`,
      };
    },
    extractMap: function() {
      const map = new Map();
      this.score.forEach((_, index) => {
        const data = this.getAt(index);
        map.set(data.key, data);
      });
      return map;
    },
  };
}

const Series = {
  getWindowExact,
  getWindowBetween,
  getSeriesFromData,
  getScoreSeriesWithExtra,
};

module.exports = Series;
