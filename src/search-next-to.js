function searchIndex(array, value, start = 0, right) {
  if (!array.length) return -1;
  let end = array.length - 1;
  if (value > array[end].x) return right ? -1 : end;
  if (value < array[start].x) return right ? start : -1;

  while (start <= end) {
    const middle = Math.floor((start + end) * 0.5);
    const middleValue = array[middle].x;
    if (middleValue === value) return middle;
    if (middleValue > value) end = middle - 1;
    else start = middle + 1;
  }

  return right ? start : end;
}

module.exports = searchIndex;
