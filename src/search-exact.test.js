const searchExactIndex = require('./search-exact');

const at = (time, value) => ({ x: time, y: value });

test('it finds the index at given time', () => {
  const series = [at('1'), at('2'), at('4'), at('5'), at('6'), at('7')];
  expect(searchExactIndex(series, '4', 0)).toEqual(2);
  expect(searchExactIndex(series, '2', 0)).toEqual(1);
  expect(searchExactIndex(series, '7', 0)).toEqual(5);
  expect(searchExactIndex(series, '1', 0)).toEqual(0);
});

test('it returns -1 on boundaries', () => {
  const series = [at('2'), at('3'), at('4'), at('5'), at('6'), at('7')];
  expect(searchExactIndex(series, '9', 0)).toEqual(-1);
  expect(searchExactIndex(series, '1', 0)).toEqual(-1);
});
