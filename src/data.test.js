const inputData = require('../data.json');

function isOrdered(series) {
  for (let i = 1; i < series.length; i += 1) {
    const prev = series[i - 1];
    const item = series[i];
    if (prev.x < prev.x) return false;
  }
  return true;
}
function areCoupled(scoreSeries, extraSeries) {
  for (let i = 0; i < scoreSeries.length; i += 1) {
    const score = scoreSeries[i];
    const extra = extraSeries[i];
    if (score.x !== extra.x) return false;
  }
  return true;
}

test('series is ordered', () => {
  const scoreSeries = inputData.data[0].details[0].series;
  const extraSeries = inputData.data[0].details[1].series;
  expect(isOrdered(scoreSeries)).toBeTruthy();
  expect(isOrdered(extraSeries)).toBeTruthy();
});

test('series in details are coupled', () => {
  const scoreSeries = inputData.data[0].details[0].series;
  const extraSeries = inputData.data[0].details[1].series;
  expect(areCoupled(scoreSeries, extraSeries)).toBeTruthy();
});
